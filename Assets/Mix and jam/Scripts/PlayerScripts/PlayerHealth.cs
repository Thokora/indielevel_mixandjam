﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour
{
    public GameObject PanelGameover;
    public Image UiAir;
    public float startingHealth = 100f;
    public static float Health;

    void Awake()
    {
        Health = startingHealth;
    }

    void Update()
    {
        UiAir.fillAmount = Health/100;
        if (Health <= 0f)
        {
            Movement.playerMoveInstance.rb.isKinematic = true;
            PanelGameover.SetActive(true);
        }
    }

}
