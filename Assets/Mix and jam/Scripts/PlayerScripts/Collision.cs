﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collision : MonoBehaviour
{

    [Header("Layers")]
    public LayerMask groundLayer;

    [Space]

    public bool onGround;
    public bool onWall;
    public bool onRightWall;
    public bool onLeftWall;
    public int wallSide;

    [Space]

    [Header("Collision")]

    public float collisionRadius = 0.25f;
    public Vector3 bottomOffset, rightOffset, leftOffset;
    private Color debugCollisionColor = Color.red;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        var collOnGround = Physics.OverlapSphere(transform.position + bottomOffset, collisionRadius, groundLayer, QueryTriggerInteraction.UseGlobal);
        onGround = collOnGround.Length > 0;

        var collOnRightWall = Physics.OverlapSphere(transform.position + rightOffset, collisionRadius, groundLayer, QueryTriggerInteraction.UseGlobal);
        var collOnLeftWall = Physics.OverlapSphere(transform.position + leftOffset, collisionRadius, groundLayer, QueryTriggerInteraction.UseGlobal);
        onWall = collOnRightWall.Length > 0 || collOnLeftWall.Length > 0;

        onRightWall = collOnRightWall.Length > 0;
        onLeftWall = collOnLeftWall.Length > 0;

        wallSide = onRightWall ? -1 : 1;

        /*onGround = Physics2D.OverlapCircle((Vector2)transform.position + bottomOffset, collisionRadius, groundLayer);
        onWall = Physics2D.OverlapCircle((Vector2)transform.position + rightOffset, collisionRadius, groundLayer) 
            || Physics2D.OverlapCircle((Vector2)transform.position + leftOffset, collisionRadius, groundLayer);

        onRightWall = Physics2D.OverlapCircle((Vector2)transform.position + rightOffset, collisionRadius, groundLayer);
        onLeftWall = Physics2D.OverlapCircle((Vector2)transform.position + leftOffset, collisionRadius, groundLayer);*/

    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;

        var positions = new Vector3[] { bottomOffset, rightOffset, leftOffset };

        Gizmos.DrawWireSphere(transform.position  + bottomOffset, collisionRadius);
        Gizmos.DrawWireSphere(transform.position + rightOffset, collisionRadius);
        Gizmos.DrawWireSphere(transform.position + leftOffset, collisionRadius);
    }
}
