﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterBehaviour : MonoBehaviour
{
    public float waterMaxResistance = 3400;
    public float waterMinResistance = 2500;
    public float waterResistance;
    public float speedInWater = 4f;
    public bool isInside;

    private float normalSpeed;

    private void Start()
    {
        normalSpeed = Movement.playerMoveInstance.speed;
        waterResistance = waterMaxResistance;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            isInside = true;
            StartCoroutine(CountDown());
            Movement.playerMoveInstance.rb.AddForce((Vector3.up * -5) * Time.deltaTime, ForceMode.Impulse);
            StartCoroutine(Breathing());
        }

    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            isInside = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            isInside = false;
            waterResistance = waterMaxResistance;
            Movement.playerMoveInstance.rb.AddForce((Vector3.up * -200) * Time.deltaTime, ForceMode.Impulse);
        }
    }

    private void FixedUpdate()
    {
        if (isInside)
        {
            Movement.playerMoveInstance.rb.AddForce(Vector3.up * waterResistance * Time.deltaTime, ForceMode.Acceleration);
            Movement.playerMoveInstance.speed = speedInWater;

            if (Movement.playerMoveInstance.groundTouch)
            {
                Movement.playerMoveInstance.rb.AddForce((Vector3.up * 5) * Time.deltaTime, ForceMode.Impulse);
                waterResistance = waterMaxResistance;
            }
        }
        else
        {
            Movement.playerMoveInstance.speed = normalSpeed;
        }
        Debug.Log("Speed: " + Movement.playerMoveInstance.speed);
    }
    IEnumerator CountDown()
    {
        if (waterResistance > waterMinResistance)
        {
            yield return new WaitForSecondsRealtime(0.5f);
            waterResistance -= 50f;
            StartCoroutine(CountDown());
        }
    }

    IEnumerator Breathing()
    {
        Debug.Log(PlayerHealth.Health);
        if (isInside && PlayerHealth.Health > 0)
        {
            PlayerHealth.Health -= 10;
            yield return new WaitForSecondsRealtime(1.0f);
            StartCoroutine(Breathing());

        }
        else if (!isInside && PlayerHealth.Health < 99)
        {
            PlayerHealth.Health += 10;
            yield return new WaitForSecondsRealtime(1.0f);
            StartCoroutine(Breathing());
        }


    }

}
