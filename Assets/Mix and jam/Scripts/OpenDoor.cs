﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenDoor : MonoBehaviour
{
    bool buttonPressed = false;
    Animator buttonAnim;
    public Animator doorAnim;

    private void Start()
    {
        buttonAnim = gameObject.GetComponent<Animator>();
    }

    private void OnCollisionEnter(UnityEngine.Collision collision)
    {
        if (collision.gameObject.layer == 10) //Weapon layer
        {
            buttonAnim.SetTrigger("Pressed");
            buttonPressed = true;
            SetDoorAnim();
        }
    }

    void SetDoorAnim()
    {
        doorAnim.SetBool("Open", buttonPressed);
    }


     
}
