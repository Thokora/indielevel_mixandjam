﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TryAgainButton : MonoBehaviour
{
    public Color FadeColor;

    public void TrayAgain()
    {
        Initiate.Fade("2.5D", FadeColor, 2f);
    }
}
