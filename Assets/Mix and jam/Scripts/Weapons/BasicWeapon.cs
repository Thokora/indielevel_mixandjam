﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

abstract public class BasicWeapon : MonoBehaviour
{
    #region Weapon Funcionality
    public bool right;

    abstract public void IdentifyWeapon();

    internal GameObject ThrowWeapon(Transform ThrowPlace)
    {
        if (ThrowPlace.localPosition.x > 0)
        {
            right = true;
        }
        else
        {
            right = false;
        }
        return Instantiate(this, ThrowPlace.position, Quaternion.identity).gameObject;
    }

    #endregion

}
