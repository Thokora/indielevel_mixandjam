﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GranadeWeapon : BasicWeapon
{
    public GameObject explosionParticle;
    GameObject explosion = null;
    Rigidbody rb;

    float torqueForce = 2.5f;
    float radius = 30f;
    float powerExplotion = 50f;
    float lift = 70f;
    int stoploop = 0;

    float force = 2;

    public override void IdentifyWeapon() 
    {
        Debug.Log("Granade");
        ShootGranate();
    }
    private void Update()
    {
    }

    void ShootGranate()
    {

        Vector3 mousePos = Input.mousePosition;
        mousePos.z = Camera.main.nearClipPlane;
        Vector3 point = Camera.main.ScreenToWorldPoint(mousePos);
        rb = gameObject.GetComponent<Rigidbody>();

        Vector3 direction = point;
        Vector3 forceDirection = (point - transform.position) * force;

        rb.AddForceAtPosition(forceDirection, direction, ForceMode.Impulse);
    }
    private void OnCollisionEnter(UnityEngine.Collision collision)
    {
        if (collision.gameObject.layer == 8) //Ground layer
        {
            if (stoploop == 0)
            {
                stoploop = 1;
                StartCoroutine(WaitUntilStop());
            }
        }
    }

    IEnumerator WaitUntilStop()
    {
        if (explosion == null)
        {
            yield return new WaitUntil(() => rb.velocity.magnitude <= 0.1f);

            yield return new WaitForSecondsRealtime(1f);
        
            Vector3 granadePos = transform.position;

            Collider[] colliders = Physics.OverlapSphere(granadePos, radius);
            foreach (Collider hit in colliders)
            {
                if (hit.GetComponent<Rigidbody>())
                {
                    hit.GetComponent<Rigidbody>().AddExplosionForce(powerExplotion, granadePos, radius, lift);
                }
            }

            explosion = Instantiate(explosionParticle, this.gameObject.transform, false);
            gameObject.transform.GetChild(0).gameObject.SetActive(false);

            yield return new WaitForSecondsRealtime(1f);
            Destroy(gameObject);
        }

    }
}
