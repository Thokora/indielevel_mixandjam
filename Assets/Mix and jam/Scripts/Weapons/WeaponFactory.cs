﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponFactory : MonoBehaviour
{
    public GranadeWeapon grandeRef;
    public KnifeWeapon knifeRef;
    public Transform initPosition;

    Vector3 placeFromIsLaunched;

    Dictionary<WeaponCode, BasicWeapon> codeToWeaponRef;

    // Start is called before the first frame update
    void Start()
    {
        SetupFactory();
    }

    void SetupFactory()
    {
        codeToWeaponRef = new Dictionary<WeaponCode, BasicWeapon>()
        {
            {WeaponCode.WPCode_Granade,grandeRef},
            {WeaponCode.WPCode_Knife, knifeRef}
        };
    }

    public BasicWeapon CreateWeapon(WeaponCode code) // instance the weapon
    {
        return codeToWeaponRef[code].ThrowWeapon(initPosition).GetComponent<BasicWeapon>();
    }

}

public enum WeaponCode
{
    WPCode_Granade,
    WPCode_Knife
}
