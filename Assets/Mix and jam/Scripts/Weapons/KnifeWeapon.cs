﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KnifeWeapon : BasicWeapon
{
    Rigidbody rb;
    float direction = 20;

    public override void IdentifyWeapon()
    {
        rb = gameObject.GetComponent<Rigidbody>();

        if (right)
        {
            rb.AddForce(new Vector3(direction,0, 0),ForceMode.Impulse);  
        }
        else
        {
            rb.AddForce(new Vector3(-direction,0, 0), ForceMode.Impulse);
        }
        Debug.Log("Knife");
    }
}
