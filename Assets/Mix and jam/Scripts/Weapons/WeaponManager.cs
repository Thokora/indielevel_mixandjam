﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponManager : MonoBehaviour
{
    WeaponFactory wpFactory;
    WeaponCode wpCode = WeaponCode.WPCode_Granade;

    List<BasicWeapon> bscWps = new List<BasicWeapon>();

    private void Start()
    {
        SetupWpMng();
    }

    private void Update()
    {
        KeyDownedToWeapon();
    }

    void SetupWpMng()
    {
        wpFactory = FindObjectOfType<WeaponFactory>();
    }

    void KeyDownedToWeapon()
    {
        if (Input.GetButtonDown("Fire2"))
        {
            wpCode = WeaponCode.WPCode_Granade;
            Shoot();
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2) || Input.GetKeyDown(KeyCode.Keypad2))
        {
            wpCode = WeaponCode.WPCode_Knife;
            Shoot();
        }
    }
    void Shoot()
    {
        BasicWeapon bscWp = wpFactory.CreateWeapon(wpCode);
        bscWps.Add(bscWp);
        bscWp.IdentifyWeapon();
    }
}
