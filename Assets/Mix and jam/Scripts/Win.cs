﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Win : MonoBehaviour
{
    public Color fadeColor;
    public GameObject WinPanel;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            WinPanel.SetActive(true);
            Movement.playerMoveInstance.rb.isKinematic = true;
            StartCoroutine(WaitAndChangeScene());
        }
    }
    IEnumerator WaitAndChangeScene()
    {
        yield return new WaitForSecondsRealtime(5f);
        Initiate.Fade("2.5D", fadeColor, 5f);
    }
}
